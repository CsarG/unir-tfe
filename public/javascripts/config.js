exports.url = "HTTP://127.0.0.1:8545"; //Dev: HTTP://127.0.0.1:8545 Unir: http://138.4.143.82:8545
exports.password = ""; //Dev: "" Unir: Alumnos_2018_Q4_IKx5srvT
exports.gasLimit = 996721975;

exports.accounts = [
  {
    id: 1,
    account: '0x6be4f4D9034c88e5D0bB2Ca78F32b9255F71452C',
    unlocked: false
  },
  {
    id: 2,
    account: '0xE762E386F1870217FDC2f668aD37ee2C9e0f201c',
    unlocked: false
  },
  {
    id: 3,
    account: '0x088672a67d2c90e36568753f97775ECE4ED8D963',
    unlocked: false
  },
  {
    id: 4,
    account: '0xd3A09e4c8e10163C93E995e73b743af8541671ce',
    unlocked: false
  }
];

exports.contracts = [
  {
    detalle: 'IronToken',
    contract: '0xFE7FB9713668F9eDB8abCc6e1db662E61c80bd3F'
  },
  {
    detalle: 'Empresa',
    contract: '0xC9E7D2C95Ff097463cA6bfD8834E532BcEba3f39',
  },
  {
    detalle: 'Propietario',
    contract: '0x2F09Ee6F236DDfb52B407330765f97D530F2769D',
  },
  {
    detalle: 'Solicitud',
    contract: '0x7cab4e9617B6DeB407C66a5d026fd88F114305c7',
  },
];

