var express = require('express');
var router = express.Router();
var IronToken = require('../public/javascripts/ironToken');
var Empresa = require('../public/javascripts/empresa');
var Propietario = require('../public/javascripts/propietario');
var Solicitud = require('../public/javascripts/empresa');


/* GET home page. */
router.get('/', (req, res, next) => {

  Empresa.getEmpresa().then((result) => {
    res.render('empresa', { title: 'Express UNIR TFE', mensaje: `Cuenta ethereum :${result}` });
  }, (error) => {
    console.log(error);
  });


});

module.exports = router;
